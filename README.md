<!--
  SPDX-FileCopyrightText: 2019 Free Software Foundation Europe e.V.
  SPDX-License-Identifier: CC-BY-SA-4.0
-->

# REUSE Logotype

The REUSE initiative logotype can be used standalone or with the text
appearing underneath. Use whichever works in your situation. If using
the logotype by itself without the text, it's important the text still
appear in connection to it somewhere.
